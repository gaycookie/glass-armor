![Mod Banner](GlassArmorBanner.png)

### Introduction
From the creator of [Transparent Cosmetics](https://modrinth.com/mod/transparent-cosmetics) comes Glass Armor, a mod that shares the same concept but is not dependent on the Cosmetic Armor mod.  
It is designed to be compatible with any mod that allows you to use armor as cosmetics.

### Mod Description
Glass Armor is a mod designed with PvE gameplay in mind.  
However, please note that it may be considered overpowered on PvP servers.  
If you plan to use it on a PvP server, exercise caution and balance it accordingly.

### Technical Information
- Mod Loader: Fabric
- Supported Minecraft Versions: 1.16.5, 1.17, 1.18, 1.19, 1.20

### Known Supported Mods
This list contains the mods that are known to be compatible with Glass Armor.  
If you are aware of any mods that are not included here, please feel free to inform me.

- [Cosmetic Armor](https://modrinth.com/mod/cosmetic-armor)

### Changelog
To stay updated on the latest changes, please refer to the [changelog](https://gitlab.com/gaycookie/glass-armor/-/wikis/Changelog).

### Dependencies
- [Fabric API](https://modrinth.com/mod/fabric-api)

### Included in Mod Packs
If you would like to feature this mod in your mod pack, kindly let me know and share it with me.

### Help and Support
If you have any issues or concerns, please visit the GitLab repository mentioned in the "source" section and post your problems there. We'll be more than happy to provide assistance. Additionally, you can also join our Discord server [here](https://discord.gg/6EDcPeepCe) to seek support.

I hope you enjoy using Glass Armor in your Minecraft adventures!