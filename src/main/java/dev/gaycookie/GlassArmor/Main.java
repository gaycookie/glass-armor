package dev.gaycookie.GlassArmor;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.*;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class Main implements ModInitializer {
	public static final String MODID = "glass_armor";

	public static final ArmorMaterial MATERIAL = new Material();
	public static final Item INGOT = new Item(new FabricItemSettings().maxCount(64));
	public static final Item HELMET = new GlassArmorItem(MATERIAL, ArmorItem.Type.HELMET);
	public static final Item CHESTPLATE = new GlassArmorItem(MATERIAL, ArmorItem.Type.CHESTPLATE);
	public static final Item LEGGINGS = new GlassArmorItem(MATERIAL, ArmorItem.Type.LEGGINGS);
	public static final Item BOOTS = new GlassArmorItem(MATERIAL, ArmorItem.Type.BOOTS);

	@Override
	public void onInitialize() {
		registerItems();
		createItemGroup();
	}

	public static void registerItems() {
		Registry.register(Registries.ITEM, new Identifier(MODID, "glass_ingot"), INGOT);
		Registry.register(Registries.ITEM, new Identifier(MODID, "helmet"), HELMET);
		Registry.register(Registries.ITEM, new Identifier(MODID, "chestplate"), CHESTPLATE);
		Registry.register(Registries.ITEM, new Identifier(MODID, "leggings"), LEGGINGS);
		Registry.register(Registries.ITEM, new Identifier(MODID, "boots"), BOOTS);
	}

	public static void createItemGroup() {
		var itemGroup = FabricItemGroup.builder()
			.displayName(Text.translatable("group.glass_armor.items"))
			.icon(() -> new ItemStack(CHESTPLATE))
			.build();

		Registry.register(Registries.ITEM_GROUP, new Identifier(MODID, "items"), itemGroup);
		if (Registries.ITEM_GROUP.getKey(itemGroup).isPresent()) {
			ItemGroupEvents.modifyEntriesEvent(Registries.ITEM_GROUP.getKey(itemGroup).get()).register(content -> {
				content.add(new ItemStack(INGOT));
				content.add(new ItemStack(HELMET));
				content.add(new ItemStack(CHESTPLATE));
				content.add(new ItemStack(LEGGINGS));
				content.add(new ItemStack(BOOTS));
			});
		}
	}
}
