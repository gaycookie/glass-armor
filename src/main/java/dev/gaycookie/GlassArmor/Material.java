package dev.gaycookie.GlassArmor;

import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;

public class Material implements ArmorMaterial {
	@Override
	public int getDurability(ArmorItem.Type type) {
		return 1;
	}

	@Override
	public int getProtection(ArmorItem.Type type) {
		return 1;
	}

	@Override
	public int getEnchantability() {
		return 0;
	}

	@Override
	public SoundEvent getEquipSound() {
		return SoundEvents.ITEM_ARMOR_EQUIP_CHAIN;
	}

	@Override
	public float getKnockbackResistance() {
		return 0;
	}

	@Override
	public String getName() {
		return "glass";
	}

	@Override
	public Ingredient getRepairIngredient() {
		return null;
	}

	@Override
	public float getToughness() {
		return 0;
	}
}
